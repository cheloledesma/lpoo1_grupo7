﻿namespace Vistas
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pnlLeftMain = new System.Windows.Forms.Panel();
            this.btnReservas = new System.Windows.Forms.Button();
            this.btnMaterias = new System.Windows.Forms.Button();
            this.btnProfesores = new System.Windows.Forms.Button();
            this.btnRecursos = new System.Windows.Forms.Button();
            this.btnSistema = new System.Windows.Forms.Button();
            this.pnlLeftUpMain = new System.Windows.Forms.Panel();
            this.lblNameSystem = new System.Windows.Forms.Label();
            this.pnlUpMain = new System.Windows.Forms.Panel();
            this.pnlLeftMin = new System.Windows.Forms.Panel();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.btnHome = new System.Windows.Forms.Button();
            this.pnlLeftMain.SuspendLayout();
            this.pnlLeftUpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlLeftMain
            // 
            this.pnlLeftMain.Controls.Add(this.btnHome);
            this.pnlLeftMain.Controls.Add(this.btnReservas);
            this.pnlLeftMain.Controls.Add(this.btnMaterias);
            this.pnlLeftMain.Controls.Add(this.btnProfesores);
            this.pnlLeftMain.Controls.Add(this.btnRecursos);
            this.pnlLeftMain.Controls.Add(this.btnSistema);
            this.pnlLeftMain.Controls.Add(this.pnlLeftUpMain);
            this.pnlLeftMain.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeftMain.Location = new System.Drawing.Point(0, 0);
            this.pnlLeftMain.Name = "pnlLeftMain";
            this.pnlLeftMain.Size = new System.Drawing.Size(140, 561);
            this.pnlLeftMain.TabIndex = 0;
            // 
            // btnReservas
            // 
            this.btnReservas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReservas.FlatAppearance.BorderSize = 0;
            this.btnReservas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReservas.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReservas.ForeColor = System.Drawing.Color.White;
            this.btnReservas.Image = ((System.Drawing.Image)(resources.GetObject("btnReservas.Image")));
            this.btnReservas.Location = new System.Drawing.Point(2, 477);
            this.btnReservas.Name = "btnReservas";
            this.btnReservas.Size = new System.Drawing.Size(136, 70);
            this.btnReservas.TabIndex = 7;
            this.btnReservas.Text = "Reservas";
            this.btnReservas.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnReservas.UseVisualStyleBackColor = true;
            this.btnReservas.Click += new System.EventHandler(this.btnReservas_Click);
            // 
            // btnMaterias
            // 
            this.btnMaterias.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMaterias.FlatAppearance.BorderSize = 0;
            this.btnMaterias.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaterias.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaterias.ForeColor = System.Drawing.Color.White;
            this.btnMaterias.Image = ((System.Drawing.Image)(resources.GetObject("btnMaterias.Image")));
            this.btnMaterias.Location = new System.Drawing.Point(2, 408);
            this.btnMaterias.Name = "btnMaterias";
            this.btnMaterias.Size = new System.Drawing.Size(136, 70);
            this.btnMaterias.TabIndex = 6;
            this.btnMaterias.Text = "Materias";
            this.btnMaterias.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnMaterias.UseVisualStyleBackColor = true;
            this.btnMaterias.Click += new System.EventHandler(this.btnMaterias_Click);
            // 
            // btnProfesores
            // 
            this.btnProfesores.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProfesores.FlatAppearance.BorderSize = 0;
            this.btnProfesores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProfesores.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProfesores.ForeColor = System.Drawing.Color.White;
            this.btnProfesores.Image = ((System.Drawing.Image)(resources.GetObject("btnProfesores.Image")));
            this.btnProfesores.Location = new System.Drawing.Point(2, 339);
            this.btnProfesores.Name = "btnProfesores";
            this.btnProfesores.Size = new System.Drawing.Size(136, 70);
            this.btnProfesores.TabIndex = 5;
            this.btnProfesores.Text = "Profesores";
            this.btnProfesores.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnProfesores.UseVisualStyleBackColor = true;
            this.btnProfesores.Click += new System.EventHandler(this.btnProfesores_Click);
            // 
            // btnRecursos
            // 
            this.btnRecursos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRecursos.FlatAppearance.BorderSize = 0;
            this.btnRecursos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecursos.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecursos.ForeColor = System.Drawing.Color.White;
            this.btnRecursos.Image = ((System.Drawing.Image)(resources.GetObject("btnRecursos.Image")));
            this.btnRecursos.Location = new System.Drawing.Point(2, 270);
            this.btnRecursos.Name = "btnRecursos";
            this.btnRecursos.Size = new System.Drawing.Size(136, 70);
            this.btnRecursos.TabIndex = 4;
            this.btnRecursos.Text = "Recursos";
            this.btnRecursos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnRecursos.UseVisualStyleBackColor = true;
            this.btnRecursos.Click += new System.EventHandler(this.btnRecursos_Click);
            // 
            // btnSistema
            // 
            this.btnSistema.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSistema.FlatAppearance.BorderSize = 0;
            this.btnSistema.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSistema.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSistema.ForeColor = System.Drawing.Color.White;
            this.btnSistema.Image = ((System.Drawing.Image)(resources.GetObject("btnSistema.Image")));
            this.btnSistema.Location = new System.Drawing.Point(2, 201);
            this.btnSistema.Name = "btnSistema";
            this.btnSistema.Size = new System.Drawing.Size(136, 70);
            this.btnSistema.TabIndex = 3;
            this.btnSistema.Text = "Sistema";
            this.btnSistema.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSistema.UseVisualStyleBackColor = true;
            this.btnSistema.Click += new System.EventHandler(this.btnSistema_Click);
            // 
            // pnlLeftUpMain
            // 
            this.pnlLeftUpMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(121)))), ((int)(((byte)(251)))));
            this.pnlLeftUpMain.Controls.Add(this.lblNameSystem);
            this.pnlLeftUpMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLeftUpMain.Location = new System.Drawing.Point(0, 0);
            this.pnlLeftUpMain.Name = "pnlLeftUpMain";
            this.pnlLeftUpMain.Size = new System.Drawing.Size(140, 60);
            this.pnlLeftUpMain.TabIndex = 2;
            // 
            // lblNameSystem
            // 
            this.lblNameSystem.AutoSize = true;
            this.lblNameSystem.Enabled = false;
            this.lblNameSystem.Font = new System.Drawing.Font("Monotype Corsiva", 35F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameSystem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.lblNameSystem.Location = new System.Drawing.Point(33, 2);
            this.lblNameSystem.Name = "lblNameSystem";
            this.lblNameSystem.Size = new System.Drawing.Size(74, 56);
            this.lblNameSystem.TabIndex = 5;
            this.lblNameSystem.Text = "G7";
            // 
            // pnlUpMain
            // 
            this.pnlUpMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUpMain.Location = new System.Drawing.Point(140, 0);
            this.pnlUpMain.Name = "pnlUpMain";
            this.pnlUpMain.Size = new System.Drawing.Size(644, 60);
            this.pnlUpMain.TabIndex = 1;
            // 
            // pnlLeftMin
            // 
            this.pnlLeftMin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(121)))), ((int)(((byte)(251)))));
            this.pnlLeftMin.Location = new System.Drawing.Point(138, 132);
            this.pnlLeftMin.Name = "pnlLeftMin";
            this.pnlLeftMin.Size = new System.Drawing.Size(5, 70);
            this.pnlLeftMin.TabIndex = 2;
            // 
            // pnlBody
            // 
            this.pnlBody.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(221)))), ((int)(((byte)(223)))));
            this.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBody.Location = new System.Drawing.Point(140, 60);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Size = new System.Drawing.Size(644, 501);
            this.pnlBody.TabIndex = 3;
            // 
            // btnHome
            // 
            this.btnHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.Color.White;
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.Location = new System.Drawing.Point(2, 132);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(136, 70);
            this.btnHome.TabIndex = 8;
            this.btnHome.Text = "Home";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // frmMain2
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pnlBody);
            this.Controls.Add(this.pnlLeftMin);
            this.Controls.Add(this.pnlUpMain);
            this.Controls.Add(this.pnlLeftMain);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMain2";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain2_Load);
            this.pnlLeftMain.ResumeLayout(false);
            this.pnlLeftUpMain.ResumeLayout(false);
            this.pnlLeftUpMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlLeftMain;
        private System.Windows.Forms.Panel pnlLeftUpMain;
        private System.Windows.Forms.Panel pnlUpMain;
        private System.Windows.Forms.Label lblNameSystem;
        private System.Windows.Forms.Button btnSistema;
        private System.Windows.Forms.Button btnRecursos;
        private System.Windows.Forms.Button btnProfesores;
        private System.Windows.Forms.Button btnMaterias;
        private System.Windows.Forms.Button btnReservas;
        private System.Windows.Forms.Panel pnlLeftMin;
        private System.Windows.Forms.Panel pnlBody;
        private System.Windows.Forms.Button btnHome;
    }
}