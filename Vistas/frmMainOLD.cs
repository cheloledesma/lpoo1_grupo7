﻿using System;
using System.Windows.Forms;

namespace Vistas
{
    public partial class frmMainOLD : Form
    {
        public frmMainOLD()
        {
            InitializeComponent();
        }

        private void pbCloseMain_Click(object sender, EventArgs e)
        {
            SalirSistema();
        }

        public void SalirSistema()
        {
            if (MessageBox.Show("Está seguro que desea salir del sistema?", "Salir del sistema",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SalirSistema();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            var example = new UControls.LogIn.ULogIn();
            example.Dock = DockStyle.Fill;
            
            pnlBodyMain.Controls.Add(example);
        }
    }
}
