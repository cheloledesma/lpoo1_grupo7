﻿namespace Vistas.UControls.Profesores
{
    partial class UcMenuProfesores
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UcMenuProfesores));
            this.pnlProfesores = new System.Windows.Forms.Panel();
            this.pnlSearchProfesor = new System.Windows.Forms.Panel();
            this.pbSearchProfesor = new System.Windows.Forms.PictureBox();
            this.lblDescriptionSearchRecurso = new System.Windows.Forms.Label();
            this.lblSearchProfesor = new System.Windows.Forms.Label();
            this.pnlTitleSection = new System.Windows.Forms.Panel();
            this.lblNameSection = new System.Windows.Forms.Label();
            this.pnlAddProfesor = new System.Windows.Forms.Panel();
            this.lblDescriptionAddProfesor = new System.Windows.Forms.Label();
            this.pbAddProfesor = new System.Windows.Forms.PictureBox();
            this.lblAddProfesor = new System.Windows.Forms.Label();
            this.pnlHelpSection = new System.Windows.Forms.Panel();
            this.pbHelp = new System.Windows.Forms.PictureBox();
            this.lblDescriptionHelp = new System.Windows.Forms.Label();
            this.lblHelp = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlProfesores.SuspendLayout();
            this.pnlSearchProfesor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSearchProfesor)).BeginInit();
            this.pnlTitleSection.SuspendLayout();
            this.pnlAddProfesor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddProfesor)).BeginInit();
            this.pnlHelpSection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHelp)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlProfesores
            // 
            this.pnlProfesores.BackColor = System.Drawing.SystemColors.Control;
            this.pnlProfesores.Controls.Add(this.panel3);
            this.pnlProfesores.Controls.Add(this.pnlSearchProfesor);
            this.pnlProfesores.Controls.Add(this.pnlTitleSection);
            this.pnlProfesores.Controls.Add(this.pnlAddProfesor);
            this.pnlProfesores.Controls.Add(this.pnlHelpSection);
            this.pnlProfesores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlProfesores.Location = new System.Drawing.Point(0, 0);
            this.pnlProfesores.Name = "pnlProfesores";
            this.pnlProfesores.Size = new System.Drawing.Size(727, 375);
            this.pnlProfesores.TabIndex = 6;
            // 
            // pnlSearchProfesor
            // 
            this.pnlSearchProfesor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(118)))), ((int)(((byte)(190)))));
            this.pnlSearchProfesor.Controls.Add(this.pbSearchProfesor);
            this.pnlSearchProfesor.Controls.Add(this.lblDescriptionSearchRecurso);
            this.pnlSearchProfesor.Controls.Add(this.lblSearchProfesor);
            this.pnlSearchProfesor.Location = new System.Drawing.Point(175, 46);
            this.pnlSearchProfesor.Name = "pnlSearchProfesor";
            this.pnlSearchProfesor.Size = new System.Drawing.Size(133, 88);
            this.pnlSearchProfesor.TabIndex = 4;
            // 
            // pbSearchProfesor
            // 
            this.pbSearchProfesor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbSearchProfesor.Image = ((System.Drawing.Image)(resources.GetObject("pbSearchProfesor.Image")));
            this.pbSearchProfesor.Location = new System.Drawing.Point(99, 38);
            this.pbSearchProfesor.Name = "pbSearchProfesor";
            this.pbSearchProfesor.Size = new System.Drawing.Size(30, 30);
            this.pbSearchProfesor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSearchProfesor.TabIndex = 3;
            this.pbSearchProfesor.TabStop = false;
            // 
            // lblDescriptionSearchRecurso
            // 
            this.lblDescriptionSearchRecurso.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionSearchRecurso.ForeColor = System.Drawing.Color.White;
            this.lblDescriptionSearchRecurso.Location = new System.Drawing.Point(0, 32);
            this.lblDescriptionSearchRecurso.Name = "lblDescriptionSearchRecurso";
            this.lblDescriptionSearchRecurso.Size = new System.Drawing.Size(98, 58);
            this.lblDescriptionSearchRecurso.TabIndex = 2;
            this.lblDescriptionSearchRecurso.Text = "Buscar un Profesor en el sistema";
            this.lblDescriptionSearchRecurso.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblSearchProfesor
            // 
            this.lblSearchProfesor.AutoSize = true;
            this.lblSearchProfesor.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchProfesor.ForeColor = System.Drawing.Color.White;
            this.lblSearchProfesor.Location = new System.Drawing.Point(3, 11);
            this.lblSearchProfesor.Name = "lblSearchProfesor";
            this.lblSearchProfesor.Size = new System.Drawing.Size(111, 17);
            this.lblSearchProfesor.TabIndex = 1;
            this.lblSearchProfesor.Text = "Search Profesor";
            // 
            // pnlTitleSection
            // 
            this.pnlTitleSection.BackColor = System.Drawing.SystemColors.Control;
            this.pnlTitleSection.Controls.Add(this.lblNameSection);
            this.pnlTitleSection.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleSection.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleSection.Name = "pnlTitleSection";
            this.pnlTitleSection.Size = new System.Drawing.Size(727, 43);
            this.pnlTitleSection.TabIndex = 3;
            // 
            // lblNameSection
            // 
            this.lblNameSection.AutoSize = true;
            this.lblNameSection.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameSection.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.lblNameSection.Location = new System.Drawing.Point(10, 11);
            this.lblNameSection.Name = "lblNameSection";
            this.lblNameSection.Size = new System.Drawing.Size(86, 21);
            this.lblNameSection.TabIndex = 1;
            this.lblNameSection.Text = "Profesores";
            // 
            // pnlAddProfesor
            // 
            this.pnlAddProfesor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(195)))), ((int)(((byte)(77)))));
            this.pnlAddProfesor.Controls.Add(this.lblDescriptionAddProfesor);
            this.pnlAddProfesor.Controls.Add(this.pbAddProfesor);
            this.pnlAddProfesor.Controls.Add(this.lblAddProfesor);
            this.pnlAddProfesor.Location = new System.Drawing.Point(14, 46);
            this.pnlAddProfesor.Name = "pnlAddProfesor";
            this.pnlAddProfesor.Size = new System.Drawing.Size(133, 88);
            this.pnlAddProfesor.TabIndex = 2;
            // 
            // lblDescriptionAddProfesor
            // 
            this.lblDescriptionAddProfesor.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionAddProfesor.ForeColor = System.Drawing.Color.White;
            this.lblDescriptionAddProfesor.Location = new System.Drawing.Point(3, 32);
            this.lblDescriptionAddProfesor.Name = "lblDescriptionAddProfesor";
            this.lblDescriptionAddProfesor.Size = new System.Drawing.Size(92, 58);
            this.lblDescriptionAddProfesor.TabIndex = 1;
            this.lblDescriptionAddProfesor.Text = "Alta de Profesor al sistema";
            this.lblDescriptionAddProfesor.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pbAddProfesor
            // 
            this.pbAddProfesor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbAddProfesor.Image = ((System.Drawing.Image)(resources.GetObject("pbAddProfesor.Image")));
            this.pbAddProfesor.Location = new System.Drawing.Point(98, 38);
            this.pbAddProfesor.Name = "pbAddProfesor";
            this.pbAddProfesor.Size = new System.Drawing.Size(30, 30);
            this.pbAddProfesor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAddProfesor.TabIndex = 0;
            this.pbAddProfesor.TabStop = false;
            // 
            // lblAddProfesor
            // 
            this.lblAddProfesor.AutoSize = true;
            this.lblAddProfesor.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddProfesor.ForeColor = System.Drawing.Color.White;
            this.lblAddProfesor.Location = new System.Drawing.Point(3, 11);
            this.lblAddProfesor.Name = "lblAddProfesor";
            this.lblAddProfesor.Size = new System.Drawing.Size(93, 17);
            this.lblAddProfesor.TabIndex = 0;
            this.lblAddProfesor.Text = "Add Profesor";
            // 
            // pnlHelpSection
            // 
            this.pnlHelpSection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(118)))), ((int)(((byte)(190)))));
            this.pnlHelpSection.Controls.Add(this.pbHelp);
            this.pnlHelpSection.Controls.Add(this.lblDescriptionHelp);
            this.pnlHelpSection.Controls.Add(this.lblHelp);
            this.pnlHelpSection.Location = new System.Drawing.Point(336, 46);
            this.pnlHelpSection.Name = "pnlHelpSection";
            this.pnlHelpSection.Size = new System.Drawing.Size(133, 88);
            this.pnlHelpSection.TabIndex = 1;
            // 
            // pbHelp
            // 
            this.pbHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbHelp.Image = ((System.Drawing.Image)(resources.GetObject("pbHelp.Image")));
            this.pbHelp.Location = new System.Drawing.Point(99, 38);
            this.pbHelp.Name = "pbHelp";
            this.pbHelp.Size = new System.Drawing.Size(30, 30);
            this.pbHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbHelp.TabIndex = 3;
            this.pbHelp.TabStop = false;
            // 
            // lblDescriptionHelp
            // 
            this.lblDescriptionHelp.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionHelp.ForeColor = System.Drawing.Color.White;
            this.lblDescriptionHelp.Location = new System.Drawing.Point(0, 32);
            this.lblDescriptionHelp.Name = "lblDescriptionHelp";
            this.lblDescriptionHelp.Size = new System.Drawing.Size(98, 58);
            this.lblDescriptionHelp.TabIndex = 2;
            this.lblDescriptionHelp.Text = "Documentación sobre la sección actual";
            this.lblDescriptionHelp.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblHelp
            // 
            this.lblHelp.AutoSize = true;
            this.lblHelp.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelp.ForeColor = System.Drawing.Color.White;
            this.lblHelp.Location = new System.Drawing.Point(3, 11);
            this.lblHelp.Name = "lblHelp";
            this.lblHelp.Size = new System.Drawing.Size(40, 17);
            this.lblHelp.TabIndex = 1;
            this.lblHelp.Text = "Help";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.pictureBox3);
            this.panel3.Controls.Add(this.label6);
            this.panel3.ForeColor = System.Drawing.SystemColors.Control;
            this.panel3.Location = new System.Drawing.Point(497, 46);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(133, 88);
            this.panel3.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(3, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 58);
            this.label5.TabIndex = 1;
            this.label5.Text = "Función no disponible";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(98, 38);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(30, 30);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(3, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Not Available";
            // 
            // UcMenuProfesores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlProfesores);
            this.Name = "UcMenuProfesores";
            this.Size = new System.Drawing.Size(727, 375);
            this.Load += new System.EventHandler(this.UcMenuProfesores_Load);
            this.pnlProfesores.ResumeLayout(false);
            this.pnlSearchProfesor.ResumeLayout(false);
            this.pnlSearchProfesor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSearchProfesor)).EndInit();
            this.pnlTitleSection.ResumeLayout(false);
            this.pnlTitleSection.PerformLayout();
            this.pnlAddProfesor.ResumeLayout(false);
            this.pnlAddProfesor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddProfesor)).EndInit();
            this.pnlHelpSection.ResumeLayout(false);
            this.pnlHelpSection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHelp)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlProfesores;
        private System.Windows.Forms.Panel pnlSearchProfesor;
        private System.Windows.Forms.PictureBox pbSearchProfesor;
        private System.Windows.Forms.Label lblDescriptionSearchRecurso;
        private System.Windows.Forms.Label lblSearchProfesor;
        private System.Windows.Forms.Panel pnlTitleSection;
        private System.Windows.Forms.Label lblNameSection;
        private System.Windows.Forms.Panel pnlAddProfesor;
        private System.Windows.Forms.Label lblDescriptionAddProfesor;
        private System.Windows.Forms.PictureBox pbAddProfesor;
        private System.Windows.Forms.Label lblAddProfesor;
        private System.Windows.Forms.Panel pnlHelpSection;
        private System.Windows.Forms.PictureBox pbHelp;
        private System.Windows.Forms.Label lblDescriptionHelp;
        private System.Windows.Forms.Label lblHelp;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label6;
    }
}
