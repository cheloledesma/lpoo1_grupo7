﻿namespace Vistas.UControls.Recursos
{
    partial class UcMenuRecursos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UcMenuRecursos));
            this.pnlMenuRecurso = new System.Windows.Forms.Panel();
            this.pnlSearchRecurso = new System.Windows.Forms.Panel();
            this.pbSearchRecurso = new System.Windows.Forms.PictureBox();
            this.lblDescriptionSearchRecurso = new System.Windows.Forms.Label();
            this.lblSearchRecurso = new System.Windows.Forms.Label();
            this.pnlTitleSection = new System.Windows.Forms.Panel();
            this.lblNameSection = new System.Windows.Forms.Label();
            this.pnlAddRecurso = new System.Windows.Forms.Panel();
            this.lblDescriptionAddRecurso = new System.Windows.Forms.Label();
            this.pbAddRecurso = new System.Windows.Forms.PictureBox();
            this.lblAddRecurso = new System.Windows.Forms.Label();
            this.pnlHelpSection = new System.Windows.Forms.Panel();
            this.pbHelp = new System.Windows.Forms.PictureBox();
            this.lblDescriptionHelp = new System.Windows.Forms.Label();
            this.lblHelp = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlMenuRecurso.SuspendLayout();
            this.pnlSearchRecurso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSearchRecurso)).BeginInit();
            this.pnlTitleSection.SuspendLayout();
            this.pnlAddRecurso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddRecurso)).BeginInit();
            this.pnlHelpSection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHelp)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMenuRecurso
            // 
            this.pnlMenuRecurso.BackColor = System.Drawing.SystemColors.Control;
            this.pnlMenuRecurso.Controls.Add(this.panel3);
            this.pnlMenuRecurso.Controls.Add(this.pnlSearchRecurso);
            this.pnlMenuRecurso.Controls.Add(this.pnlTitleSection);
            this.pnlMenuRecurso.Controls.Add(this.pnlAddRecurso);
            this.pnlMenuRecurso.Controls.Add(this.pnlHelpSection);
            this.pnlMenuRecurso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMenuRecurso.Location = new System.Drawing.Point(0, 0);
            this.pnlMenuRecurso.Name = "pnlMenuRecurso";
            this.pnlMenuRecurso.Size = new System.Drawing.Size(727, 375);
            this.pnlMenuRecurso.TabIndex = 5;
            // 
            // pnlSearchRecurso
            // 
            this.pnlSearchRecurso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(118)))), ((int)(((byte)(190)))));
            this.pnlSearchRecurso.Controls.Add(this.pbSearchRecurso);
            this.pnlSearchRecurso.Controls.Add(this.lblDescriptionSearchRecurso);
            this.pnlSearchRecurso.Controls.Add(this.lblSearchRecurso);
            this.pnlSearchRecurso.Location = new System.Drawing.Point(175, 46);
            this.pnlSearchRecurso.Name = "pnlSearchRecurso";
            this.pnlSearchRecurso.Size = new System.Drawing.Size(133, 88);
            this.pnlSearchRecurso.TabIndex = 4;
            // 
            // pbSearchRecurso
            // 
            this.pbSearchRecurso.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbSearchRecurso.Image = ((System.Drawing.Image)(resources.GetObject("pbSearchRecurso.Image")));
            this.pbSearchRecurso.Location = new System.Drawing.Point(99, 38);
            this.pbSearchRecurso.Name = "pbSearchRecurso";
            this.pbSearchRecurso.Size = new System.Drawing.Size(30, 30);
            this.pbSearchRecurso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSearchRecurso.TabIndex = 3;
            this.pbSearchRecurso.TabStop = false;
            // 
            // lblDescriptionSearchRecurso
            // 
            this.lblDescriptionSearchRecurso.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionSearchRecurso.ForeColor = System.Drawing.Color.White;
            this.lblDescriptionSearchRecurso.Location = new System.Drawing.Point(0, 32);
            this.lblDescriptionSearchRecurso.Name = "lblDescriptionSearchRecurso";
            this.lblDescriptionSearchRecurso.Size = new System.Drawing.Size(98, 58);
            this.lblDescriptionSearchRecurso.TabIndex = 2;
            this.lblDescriptionSearchRecurso.Text = "Buscar un Recurso en el sistema";
            this.lblDescriptionSearchRecurso.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblSearchRecurso
            // 
            this.lblSearchRecurso.AutoSize = true;
            this.lblSearchRecurso.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchRecurso.ForeColor = System.Drawing.Color.White;
            this.lblSearchRecurso.Location = new System.Drawing.Point(3, 11);
            this.lblSearchRecurso.Name = "lblSearchRecurso";
            this.lblSearchRecurso.Size = new System.Drawing.Size(111, 17);
            this.lblSearchRecurso.TabIndex = 1;
            this.lblSearchRecurso.Text = "Search Recurso";
            // 
            // pnlTitleSection
            // 
            this.pnlTitleSection.BackColor = System.Drawing.SystemColors.Control;
            this.pnlTitleSection.Controls.Add(this.lblNameSection);
            this.pnlTitleSection.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleSection.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleSection.Name = "pnlTitleSection";
            this.pnlTitleSection.Size = new System.Drawing.Size(727, 43);
            this.pnlTitleSection.TabIndex = 3;
            // 
            // lblNameSection
            // 
            this.lblNameSection.AutoSize = true;
            this.lblNameSection.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameSection.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.lblNameSection.Location = new System.Drawing.Point(10, 11);
            this.lblNameSection.Name = "lblNameSection";
            this.lblNameSection.Size = new System.Drawing.Size(77, 21);
            this.lblNameSection.TabIndex = 1;
            this.lblNameSection.Text = "Recursos";
            // 
            // pnlAddRecurso
            // 
            this.pnlAddRecurso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(195)))), ((int)(((byte)(77)))));
            this.pnlAddRecurso.Controls.Add(this.lblDescriptionAddRecurso);
            this.pnlAddRecurso.Controls.Add(this.pbAddRecurso);
            this.pnlAddRecurso.Controls.Add(this.lblAddRecurso);
            this.pnlAddRecurso.Location = new System.Drawing.Point(14, 46);
            this.pnlAddRecurso.Name = "pnlAddRecurso";
            this.pnlAddRecurso.Size = new System.Drawing.Size(133, 88);
            this.pnlAddRecurso.TabIndex = 2;
            // 
            // lblDescriptionAddRecurso
            // 
            this.lblDescriptionAddRecurso.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionAddRecurso.ForeColor = System.Drawing.Color.White;
            this.lblDescriptionAddRecurso.Location = new System.Drawing.Point(3, 32);
            this.lblDescriptionAddRecurso.Name = "lblDescriptionAddRecurso";
            this.lblDescriptionAddRecurso.Size = new System.Drawing.Size(92, 58);
            this.lblDescriptionAddRecurso.TabIndex = 1;
            this.lblDescriptionAddRecurso.Text = "Alta de Recurso al sistema";
            this.lblDescriptionAddRecurso.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pbAddRecurso
            // 
            this.pbAddRecurso.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbAddRecurso.Image = ((System.Drawing.Image)(resources.GetObject("pbAddRecurso.Image")));
            this.pbAddRecurso.Location = new System.Drawing.Point(98, 38);
            this.pbAddRecurso.Name = "pbAddRecurso";
            this.pbAddRecurso.Size = new System.Drawing.Size(30, 30);
            this.pbAddRecurso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAddRecurso.TabIndex = 0;
            this.pbAddRecurso.TabStop = false;
            this.pbAddRecurso.Click += new System.EventHandler(this.pbAddRecurso_Click);
            // 
            // lblAddRecurso
            // 
            this.lblAddRecurso.AutoSize = true;
            this.lblAddRecurso.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddRecurso.ForeColor = System.Drawing.Color.White;
            this.lblAddRecurso.Location = new System.Drawing.Point(3, 11);
            this.lblAddRecurso.Name = "lblAddRecurso";
            this.lblAddRecurso.Size = new System.Drawing.Size(93, 17);
            this.lblAddRecurso.TabIndex = 0;
            this.lblAddRecurso.Text = "Add Recurso";
            // 
            // pnlHelpSection
            // 
            this.pnlHelpSection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(118)))), ((int)(((byte)(190)))));
            this.pnlHelpSection.Controls.Add(this.pbHelp);
            this.pnlHelpSection.Controls.Add(this.lblDescriptionHelp);
            this.pnlHelpSection.Controls.Add(this.lblHelp);
            this.pnlHelpSection.Location = new System.Drawing.Point(336, 46);
            this.pnlHelpSection.Name = "pnlHelpSection";
            this.pnlHelpSection.Size = new System.Drawing.Size(133, 88);
            this.pnlHelpSection.TabIndex = 1;
            // 
            // pbHelp
            // 
            this.pbHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbHelp.Image = ((System.Drawing.Image)(resources.GetObject("pbHelp.Image")));
            this.pbHelp.Location = new System.Drawing.Point(99, 38);
            this.pbHelp.Name = "pbHelp";
            this.pbHelp.Size = new System.Drawing.Size(30, 30);
            this.pbHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbHelp.TabIndex = 3;
            this.pbHelp.TabStop = false;
            this.pbHelp.Click += new System.EventHandler(this.pbHelp_Click);
            // 
            // lblDescriptionHelp
            // 
            this.lblDescriptionHelp.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionHelp.ForeColor = System.Drawing.Color.White;
            this.lblDescriptionHelp.Location = new System.Drawing.Point(0, 32);
            this.lblDescriptionHelp.Name = "lblDescriptionHelp";
            this.lblDescriptionHelp.Size = new System.Drawing.Size(98, 58);
            this.lblDescriptionHelp.TabIndex = 2;
            this.lblDescriptionHelp.Text = "Documentación sobre la sección actual";
            this.lblDescriptionHelp.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblHelp
            // 
            this.lblHelp.AutoSize = true;
            this.lblHelp.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelp.ForeColor = System.Drawing.Color.White;
            this.lblHelp.Location = new System.Drawing.Point(3, 11);
            this.lblHelp.Name = "lblHelp";
            this.lblHelp.Size = new System.Drawing.Size(40, 17);
            this.lblHelp.TabIndex = 1;
            this.lblHelp.Text = "Help";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.pictureBox3);
            this.panel3.Controls.Add(this.label6);
            this.panel3.ForeColor = System.Drawing.SystemColors.Control;
            this.panel3.Location = new System.Drawing.Point(497, 46);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(133, 88);
            this.panel3.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(3, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 58);
            this.label5.TabIndex = 1;
            this.label5.Text = "Función no disponible";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(98, 38);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(30, 30);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(3, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Not Available";
            // 
            // UcMenuRecursos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMenuRecurso);
            this.Name = "UcMenuRecursos";
            this.Size = new System.Drawing.Size(727, 375);
            this.pnlMenuRecurso.ResumeLayout(false);
            this.pnlSearchRecurso.ResumeLayout(false);
            this.pnlSearchRecurso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSearchRecurso)).EndInit();
            this.pnlTitleSection.ResumeLayout(false);
            this.pnlTitleSection.PerformLayout();
            this.pnlAddRecurso.ResumeLayout(false);
            this.pnlAddRecurso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddRecurso)).EndInit();
            this.pnlHelpSection.ResumeLayout(false);
            this.pnlHelpSection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHelp)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMenuRecurso;
        private System.Windows.Forms.Panel pnlTitleSection;
        private System.Windows.Forms.Label lblNameSection;
        private System.Windows.Forms.Panel pnlAddRecurso;
        private System.Windows.Forms.Label lblDescriptionAddRecurso;
        private System.Windows.Forms.PictureBox pbAddRecurso;
        private System.Windows.Forms.Label lblAddRecurso;
        private System.Windows.Forms.Panel pnlHelpSection;
        private System.Windows.Forms.PictureBox pbHelp;
        private System.Windows.Forms.Label lblDescriptionHelp;
        private System.Windows.Forms.Label lblHelp;
        private System.Windows.Forms.Panel pnlSearchRecurso;
        private System.Windows.Forms.PictureBox pbSearchRecurso;
        private System.Windows.Forms.Label lblDescriptionSearchRecurso;
        private System.Windows.Forms.Label lblSearchRecurso;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label6;
    }
}
