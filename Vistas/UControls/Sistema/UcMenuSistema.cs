﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vistas.UControls.Sistema
{
    public partial class UcMenuSistema : UserControl
    {
        public UcMenuSistema()
        {
            InitializeComponent();
        }

        private void pbLogOut_Click(object sender, EventArgs e)
        {
            ///TODO: adicionar un MessageBox alertando si realmente quiere salir del sistema
            Application.Exit();
        }

        private void pbHelp_Click(object sender, EventArgs e)
        {
            ///TODO: adicionar un MessageBox alertando que la seccion esta en construccion
        }

        
    }
}
