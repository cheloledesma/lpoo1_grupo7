﻿namespace Vistas.UControls.Sistema
{
    partial class UcMenuSistema
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UcMenuSistema));
            this.pnlMenuSistema = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlAddRecurso = new System.Windows.Forms.Panel();
            this.lblDescriptionAddReserva = new System.Windows.Forms.Label();
            this.pbAddReserva = new System.Windows.Forms.PictureBox();
            this.lblNotAvailable = new System.Windows.Forms.Label();
            this.pnlTitleSection = new System.Windows.Forms.Panel();
            this.lblNameSection = new System.Windows.Forms.Label();
            this.pnlInLogOut = new System.Windows.Forms.Panel();
            this.lblDescriptionLogOut = new System.Windows.Forms.Label();
            this.pbLogOut = new System.Windows.Forms.PictureBox();
            this.lblLogOut = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbHelp = new System.Windows.Forms.PictureBox();
            this.lblDescriptionHelp = new System.Windows.Forms.Label();
            this.lblHelp = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlMenuSistema.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlAddRecurso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddReserva)).BeginInit();
            this.pnlTitleSection.SuspendLayout();
            this.pnlInLogOut.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogOut)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHelp)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMenuSistema
            // 
            this.pnlMenuSistema.BackColor = System.Drawing.SystemColors.Control;
            this.pnlMenuSistema.Controls.Add(this.panel6);
            this.pnlMenuSistema.Controls.Add(this.panel5);
            this.pnlMenuSistema.Controls.Add(this.panel4);
            this.pnlMenuSistema.Controls.Add(this.panel3);
            this.pnlMenuSistema.Controls.Add(this.panel2);
            this.pnlMenuSistema.Controls.Add(this.pnlAddRecurso);
            this.pnlMenuSistema.Controls.Add(this.pnlTitleSection);
            this.pnlMenuSistema.Controls.Add(this.pnlInLogOut);
            this.pnlMenuSistema.Controls.Add(this.panel1);
            this.pnlMenuSistema.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMenuSistema.Location = new System.Drawing.Point(0, 0);
            this.pnlMenuSistema.Name = "pnlMenuSistema";
            this.pnlMenuSistema.Size = new System.Drawing.Size(727, 375);
            this.pnlMenuSistema.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Controls.Add(this.label6);
            this.panel4.ForeColor = System.Drawing.SystemColors.Control;
            this.panel4.Location = new System.Drawing.Point(336, 151);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(133, 88);
            this.panel4.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(3, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 58);
            this.label5.TabIndex = 1;
            this.label5.Text = "Función no disponible";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(98, 38);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(30, 30);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(3, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Not Available";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Controls.Add(this.label4);
            this.panel3.ForeColor = System.Drawing.SystemColors.Control;
            this.panel3.Location = new System.Drawing.Point(175, 151);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(133, 88);
            this.panel3.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 58);
            this.label3.TabIndex = 1;
            this.label3.Text = "Función no disponible";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(98, 38);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 30);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(3, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Not Available";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.ForeColor = System.Drawing.SystemColors.Control;
            this.panel2.Location = new System.Drawing.Point(14, 151);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(133, 88);
            this.panel2.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 58);
            this.label1.TabIndex = 1;
            this.label1.Text = "Función no disponible";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(98, 38);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Not Available";
            // 
            // pnlAddRecurso
            // 
            this.pnlAddRecurso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.pnlAddRecurso.Controls.Add(this.lblDescriptionAddReserva);
            this.pnlAddRecurso.Controls.Add(this.pbAddReserva);
            this.pnlAddRecurso.Controls.Add(this.lblNotAvailable);
            this.pnlAddRecurso.ForeColor = System.Drawing.SystemColors.Control;
            this.pnlAddRecurso.Location = new System.Drawing.Point(336, 46);
            this.pnlAddRecurso.Name = "pnlAddRecurso";
            this.pnlAddRecurso.Size = new System.Drawing.Size(133, 88);
            this.pnlAddRecurso.TabIndex = 4;
            // 
            // lblDescriptionAddReserva
            // 
            this.lblDescriptionAddReserva.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionAddReserva.ForeColor = System.Drawing.Color.White;
            this.lblDescriptionAddReserva.Location = new System.Drawing.Point(3, 32);
            this.lblDescriptionAddReserva.Name = "lblDescriptionAddReserva";
            this.lblDescriptionAddReserva.Size = new System.Drawing.Size(92, 58);
            this.lblDescriptionAddReserva.TabIndex = 1;
            this.lblDescriptionAddReserva.Text = "Función no disponible";
            this.lblDescriptionAddReserva.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pbAddReserva
            // 
            this.pbAddReserva.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbAddReserva.Image = ((System.Drawing.Image)(resources.GetObject("pbAddReserva.Image")));
            this.pbAddReserva.Location = new System.Drawing.Point(98, 38);
            this.pbAddReserva.Name = "pbAddReserva";
            this.pbAddReserva.Size = new System.Drawing.Size(30, 30);
            this.pbAddReserva.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAddReserva.TabIndex = 0;
            this.pbAddReserva.TabStop = false;
            // 
            // lblNotAvailable
            // 
            this.lblNotAvailable.AutoSize = true;
            this.lblNotAvailable.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotAvailable.ForeColor = System.Drawing.Color.White;
            this.lblNotAvailable.Location = new System.Drawing.Point(3, 11);
            this.lblNotAvailable.Name = "lblNotAvailable";
            this.lblNotAvailable.Size = new System.Drawing.Size(100, 17);
            this.lblNotAvailable.TabIndex = 0;
            this.lblNotAvailable.Text = "Not Available";
            // 
            // pnlTitleSection
            // 
            this.pnlTitleSection.BackColor = System.Drawing.SystemColors.Control;
            this.pnlTitleSection.Controls.Add(this.lblNameSection);
            this.pnlTitleSection.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleSection.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleSection.Name = "pnlTitleSection";
            this.pnlTitleSection.Size = new System.Drawing.Size(727, 43);
            this.pnlTitleSection.TabIndex = 3;
            // 
            // lblNameSection
            // 
            this.lblNameSection.AutoSize = true;
            this.lblNameSection.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameSection.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.lblNameSection.Location = new System.Drawing.Point(10, 11);
            this.lblNameSection.Name = "lblNameSection";
            this.lblNameSection.Size = new System.Drawing.Size(70, 21);
            this.lblNameSection.TabIndex = 1;
            this.lblNameSection.Text = "Sistema";
            // 
            // pnlInLogOut
            // 
            this.pnlInLogOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(77)))), ((int)(((byte)(58)))));
            this.pnlInLogOut.Controls.Add(this.lblDescriptionLogOut);
            this.pnlInLogOut.Controls.Add(this.pbLogOut);
            this.pnlInLogOut.Controls.Add(this.lblLogOut);
            this.pnlInLogOut.Location = new System.Drawing.Point(14, 46);
            this.pnlInLogOut.Name = "pnlInLogOut";
            this.pnlInLogOut.Size = new System.Drawing.Size(133, 88);
            this.pnlInLogOut.TabIndex = 2;
            // 
            // lblDescriptionLogOut
            // 
            this.lblDescriptionLogOut.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionLogOut.ForeColor = System.Drawing.Color.White;
            this.lblDescriptionLogOut.Location = new System.Drawing.Point(3, 32);
            this.lblDescriptionLogOut.Name = "lblDescriptionLogOut";
            this.lblDescriptionLogOut.Size = new System.Drawing.Size(92, 58);
            this.lblDescriptionLogOut.TabIndex = 1;
            this.lblDescriptionLogOut.Text = "Salir del sistema de forma segura";
            this.lblDescriptionLogOut.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pbLogOut
            // 
            this.pbLogOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbLogOut.Image = ((System.Drawing.Image)(resources.GetObject("pbLogOut.Image")));
            this.pbLogOut.Location = new System.Drawing.Point(98, 38);
            this.pbLogOut.Name = "pbLogOut";
            this.pbLogOut.Size = new System.Drawing.Size(30, 30);
            this.pbLogOut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogOut.TabIndex = 0;
            this.pbLogOut.TabStop = false;
            this.pbLogOut.Click += new System.EventHandler(this.pbLogOut_Click);
            // 
            // lblLogOut
            // 
            this.lblLogOut.AutoSize = true;
            this.lblLogOut.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogOut.ForeColor = System.Drawing.Color.White;
            this.lblLogOut.Location = new System.Drawing.Point(3, 11);
            this.lblLogOut.Name = "lblLogOut";
            this.lblLogOut.Size = new System.Drawing.Size(56, 17);
            this.lblLogOut.TabIndex = 0;
            this.lblLogOut.Text = "LogOut";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(118)))), ((int)(((byte)(190)))));
            this.panel1.Controls.Add(this.pbHelp);
            this.panel1.Controls.Add(this.lblDescriptionHelp);
            this.panel1.Controls.Add(this.lblHelp);
            this.panel1.Location = new System.Drawing.Point(175, 46);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(133, 88);
            this.panel1.TabIndex = 1;
            // 
            // pbHelp
            // 
            this.pbHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbHelp.Image = ((System.Drawing.Image)(resources.GetObject("pbHelp.Image")));
            this.pbHelp.Location = new System.Drawing.Point(99, 38);
            this.pbHelp.Name = "pbHelp";
            this.pbHelp.Size = new System.Drawing.Size(30, 30);
            this.pbHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbHelp.TabIndex = 3;
            this.pbHelp.TabStop = false;
            this.pbHelp.Click += new System.EventHandler(this.pbHelp_Click);
            // 
            // lblDescriptionHelp
            // 
            this.lblDescriptionHelp.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionHelp.ForeColor = System.Drawing.Color.White;
            this.lblDescriptionHelp.Location = new System.Drawing.Point(0, 32);
            this.lblDescriptionHelp.Name = "lblDescriptionHelp";
            this.lblDescriptionHelp.Size = new System.Drawing.Size(98, 58);
            this.lblDescriptionHelp.TabIndex = 2;
            this.lblDescriptionHelp.Text = "Documentación sobre la sección actual";
            this.lblDescriptionHelp.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblHelp
            // 
            this.lblHelp.AutoSize = true;
            this.lblHelp.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelp.ForeColor = System.Drawing.Color.White;
            this.lblHelp.Location = new System.Drawing.Point(3, 11);
            this.lblHelp.Name = "lblHelp";
            this.lblHelp.Size = new System.Drawing.Size(40, 17);
            this.lblHelp.TabIndex = 1;
            this.lblHelp.Text = "Help";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.pictureBox4);
            this.panel5.Controls.Add(this.label8);
            this.panel5.ForeColor = System.Drawing.SystemColors.Control;
            this.panel5.Location = new System.Drawing.Point(497, 46);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(133, 88);
            this.panel5.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(3, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 58);
            this.label7.TabIndex = 1;
            this.label7.Text = "Función no disponible";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(98, 38);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(30, 30);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(3, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Not Available";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.pictureBox5);
            this.panel6.Controls.Add(this.label10);
            this.panel6.ForeColor = System.Drawing.SystemColors.Control;
            this.panel6.Location = new System.Drawing.Point(497, 151);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(133, 88);
            this.panel6.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(3, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 58);
            this.label9.TabIndex = 1;
            this.label9.Text = "Función no disponible";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(98, 38);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(30, 30);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(3, 11);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "Not Available";
            // 
            // UcMenuSistema
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMenuSistema);
            this.Name = "UcMenuSistema";
            this.Size = new System.Drawing.Size(727, 375);
            this.pnlMenuSistema.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlAddRecurso.ResumeLayout(false);
            this.pnlAddRecurso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddReserva)).EndInit();
            this.pnlTitleSection.ResumeLayout(false);
            this.pnlTitleSection.PerformLayout();
            this.pnlInLogOut.ResumeLayout(false);
            this.pnlInLogOut.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogOut)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHelp)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMenuSistema;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pbLogOut;
        private System.Windows.Forms.Panel pnlInLogOut;
        private System.Windows.Forms.Label lblDescriptionLogOut;
        private System.Windows.Forms.Label lblLogOut;
        private System.Windows.Forms.PictureBox pbHelp;
        private System.Windows.Forms.Label lblDescriptionHelp;
        private System.Windows.Forms.Label lblHelp;
        private System.Windows.Forms.Panel pnlTitleSection;
        private System.Windows.Forms.Label lblNameSection;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlAddRecurso;
        private System.Windows.Forms.Label lblDescriptionAddReserva;
        private System.Windows.Forms.PictureBox pbAddReserva;
        private System.Windows.Forms.Label lblNotAvailable;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label10;
    }
}
