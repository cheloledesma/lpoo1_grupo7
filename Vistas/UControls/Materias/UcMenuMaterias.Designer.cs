﻿namespace Vistas.UControls.Materias
{
    partial class UcMenuMaterias
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UcMenuMaterias));
            this.pnlProfesores = new System.Windows.Forms.Panel();
            this.pnlSearchMateria = new System.Windows.Forms.Panel();
            this.pbSearchMateria = new System.Windows.Forms.PictureBox();
            this.lblDescriptionSearchMateria = new System.Windows.Forms.Label();
            this.lblSearchMateria = new System.Windows.Forms.Label();
            this.pnlTitleSection = new System.Windows.Forms.Panel();
            this.lblNameSection = new System.Windows.Forms.Label();
            this.pnlAddMateria = new System.Windows.Forms.Panel();
            this.lblDescriptionAddMateria = new System.Windows.Forms.Label();
            this.pbAddMateria = new System.Windows.Forms.PictureBox();
            this.lblAddMateria = new System.Windows.Forms.Label();
            this.pnlHelpSection = new System.Windows.Forms.Panel();
            this.pbHelp = new System.Windows.Forms.PictureBox();
            this.lblDescriptionHelp = new System.Windows.Forms.Label();
            this.lblHelp = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlProfesores.SuspendLayout();
            this.pnlSearchMateria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSearchMateria)).BeginInit();
            this.pnlTitleSection.SuspendLayout();
            this.pnlAddMateria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddMateria)).BeginInit();
            this.pnlHelpSection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHelp)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlProfesores
            // 
            this.pnlProfesores.BackColor = System.Drawing.SystemColors.Control;
            this.pnlProfesores.Controls.Add(this.panel3);
            this.pnlProfesores.Controls.Add(this.pnlSearchMateria);
            this.pnlProfesores.Controls.Add(this.pnlTitleSection);
            this.pnlProfesores.Controls.Add(this.pnlAddMateria);
            this.pnlProfesores.Controls.Add(this.pnlHelpSection);
            this.pnlProfesores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlProfesores.Location = new System.Drawing.Point(0, 0);
            this.pnlProfesores.Name = "pnlProfesores";
            this.pnlProfesores.Size = new System.Drawing.Size(727, 375);
            this.pnlProfesores.TabIndex = 7;
            // 
            // pnlSearchMateria
            // 
            this.pnlSearchMateria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(118)))), ((int)(((byte)(190)))));
            this.pnlSearchMateria.Controls.Add(this.pbSearchMateria);
            this.pnlSearchMateria.Controls.Add(this.lblDescriptionSearchMateria);
            this.pnlSearchMateria.Controls.Add(this.lblSearchMateria);
            this.pnlSearchMateria.Location = new System.Drawing.Point(175, 46);
            this.pnlSearchMateria.Name = "pnlSearchMateria";
            this.pnlSearchMateria.Size = new System.Drawing.Size(133, 88);
            this.pnlSearchMateria.TabIndex = 4;
            // 
            // pbSearchMateria
            // 
            this.pbSearchMateria.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbSearchMateria.Image = ((System.Drawing.Image)(resources.GetObject("pbSearchMateria.Image")));
            this.pbSearchMateria.Location = new System.Drawing.Point(99, 38);
            this.pbSearchMateria.Name = "pbSearchMateria";
            this.pbSearchMateria.Size = new System.Drawing.Size(30, 30);
            this.pbSearchMateria.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSearchMateria.TabIndex = 3;
            this.pbSearchMateria.TabStop = false;
            // 
            // lblDescriptionSearchMateria
            // 
            this.lblDescriptionSearchMateria.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionSearchMateria.ForeColor = System.Drawing.Color.White;
            this.lblDescriptionSearchMateria.Location = new System.Drawing.Point(0, 32);
            this.lblDescriptionSearchMateria.Name = "lblDescriptionSearchMateria";
            this.lblDescriptionSearchMateria.Size = new System.Drawing.Size(98, 58);
            this.lblDescriptionSearchMateria.TabIndex = 2;
            this.lblDescriptionSearchMateria.Text = "Buscar un Materia en el sistema";
            this.lblDescriptionSearchMateria.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblSearchMateria
            // 
            this.lblSearchMateria.AutoSize = true;
            this.lblSearchMateria.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchMateria.ForeColor = System.Drawing.Color.White;
            this.lblSearchMateria.Location = new System.Drawing.Point(3, 11);
            this.lblSearchMateria.Name = "lblSearchMateria";
            this.lblSearchMateria.Size = new System.Drawing.Size(110, 17);
            this.lblSearchMateria.TabIndex = 1;
            this.lblSearchMateria.Text = "Search Materia";
            // 
            // pnlTitleSection
            // 
            this.pnlTitleSection.BackColor = System.Drawing.SystemColors.Control;
            this.pnlTitleSection.Controls.Add(this.lblNameSection);
            this.pnlTitleSection.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleSection.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleSection.Name = "pnlTitleSection";
            this.pnlTitleSection.Size = new System.Drawing.Size(727, 43);
            this.pnlTitleSection.TabIndex = 3;
            // 
            // lblNameSection
            // 
            this.lblNameSection.AutoSize = true;
            this.lblNameSection.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameSection.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.lblNameSection.Location = new System.Drawing.Point(10, 11);
            this.lblNameSection.Name = "lblNameSection";
            this.lblNameSection.Size = new System.Drawing.Size(78, 21);
            this.lblNameSection.TabIndex = 1;
            this.lblNameSection.Text = "Materias";
            // 
            // pnlAddMateria
            // 
            this.pnlAddMateria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(195)))), ((int)(((byte)(77)))));
            this.pnlAddMateria.Controls.Add(this.lblDescriptionAddMateria);
            this.pnlAddMateria.Controls.Add(this.pbAddMateria);
            this.pnlAddMateria.Controls.Add(this.lblAddMateria);
            this.pnlAddMateria.Location = new System.Drawing.Point(14, 46);
            this.pnlAddMateria.Name = "pnlAddMateria";
            this.pnlAddMateria.Size = new System.Drawing.Size(133, 88);
            this.pnlAddMateria.TabIndex = 2;
            // 
            // lblDescriptionAddMateria
            // 
            this.lblDescriptionAddMateria.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionAddMateria.ForeColor = System.Drawing.Color.White;
            this.lblDescriptionAddMateria.Location = new System.Drawing.Point(3, 32);
            this.lblDescriptionAddMateria.Name = "lblDescriptionAddMateria";
            this.lblDescriptionAddMateria.Size = new System.Drawing.Size(92, 58);
            this.lblDescriptionAddMateria.TabIndex = 1;
            this.lblDescriptionAddMateria.Text = "Alta de Materia al sistema";
            this.lblDescriptionAddMateria.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pbAddMateria
            // 
            this.pbAddMateria.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbAddMateria.Image = ((System.Drawing.Image)(resources.GetObject("pbAddMateria.Image")));
            this.pbAddMateria.Location = new System.Drawing.Point(98, 38);
            this.pbAddMateria.Name = "pbAddMateria";
            this.pbAddMateria.Size = new System.Drawing.Size(30, 30);
            this.pbAddMateria.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAddMateria.TabIndex = 0;
            this.pbAddMateria.TabStop = false;
            // 
            // lblAddMateria
            // 
            this.lblAddMateria.AutoSize = true;
            this.lblAddMateria.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddMateria.ForeColor = System.Drawing.Color.White;
            this.lblAddMateria.Location = new System.Drawing.Point(3, 11);
            this.lblAddMateria.Name = "lblAddMateria";
            this.lblAddMateria.Size = new System.Drawing.Size(92, 17);
            this.lblAddMateria.TabIndex = 0;
            this.lblAddMateria.Text = "Add Materia";
            // 
            // pnlHelpSection
            // 
            this.pnlHelpSection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(118)))), ((int)(((byte)(190)))));
            this.pnlHelpSection.Controls.Add(this.pbHelp);
            this.pnlHelpSection.Controls.Add(this.lblDescriptionHelp);
            this.pnlHelpSection.Controls.Add(this.lblHelp);
            this.pnlHelpSection.Location = new System.Drawing.Point(336, 46);
            this.pnlHelpSection.Name = "pnlHelpSection";
            this.pnlHelpSection.Size = new System.Drawing.Size(133, 88);
            this.pnlHelpSection.TabIndex = 1;
            // 
            // pbHelp
            // 
            this.pbHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbHelp.Image = ((System.Drawing.Image)(resources.GetObject("pbHelp.Image")));
            this.pbHelp.Location = new System.Drawing.Point(99, 38);
            this.pbHelp.Name = "pbHelp";
            this.pbHelp.Size = new System.Drawing.Size(30, 30);
            this.pbHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbHelp.TabIndex = 3;
            this.pbHelp.TabStop = false;
            // 
            // lblDescriptionHelp
            // 
            this.lblDescriptionHelp.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionHelp.ForeColor = System.Drawing.Color.White;
            this.lblDescriptionHelp.Location = new System.Drawing.Point(0, 32);
            this.lblDescriptionHelp.Name = "lblDescriptionHelp";
            this.lblDescriptionHelp.Size = new System.Drawing.Size(98, 58);
            this.lblDescriptionHelp.TabIndex = 2;
            this.lblDescriptionHelp.Text = "Documentación sobre la sección actual";
            this.lblDescriptionHelp.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblHelp
            // 
            this.lblHelp.AutoSize = true;
            this.lblHelp.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelp.ForeColor = System.Drawing.Color.White;
            this.lblHelp.Location = new System.Drawing.Point(3, 11);
            this.lblHelp.Name = "lblHelp";
            this.lblHelp.Size = new System.Drawing.Size(40, 17);
            this.lblHelp.TabIndex = 1;
            this.lblHelp.Text = "Help";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.pictureBox3);
            this.panel3.Controls.Add(this.label6);
            this.panel3.ForeColor = System.Drawing.SystemColors.Control;
            this.panel3.Location = new System.Drawing.Point(497, 46);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(133, 88);
            this.panel3.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(3, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 58);
            this.label5.TabIndex = 1;
            this.label5.Text = "Función no disponible";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(98, 38);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(30, 30);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(3, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Not Available";
            // 
            // UcMenuMaterias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlProfesores);
            this.Name = "UcMenuMaterias";
            this.Size = new System.Drawing.Size(727, 375);
            this.pnlProfesores.ResumeLayout(false);
            this.pnlSearchMateria.ResumeLayout(false);
            this.pnlSearchMateria.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSearchMateria)).EndInit();
            this.pnlTitleSection.ResumeLayout(false);
            this.pnlTitleSection.PerformLayout();
            this.pnlAddMateria.ResumeLayout(false);
            this.pnlAddMateria.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddMateria)).EndInit();
            this.pnlHelpSection.ResumeLayout(false);
            this.pnlHelpSection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHelp)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlProfesores;
        private System.Windows.Forms.Panel pnlSearchMateria;
        private System.Windows.Forms.PictureBox pbSearchMateria;
        private System.Windows.Forms.Label lblDescriptionSearchMateria;
        private System.Windows.Forms.Label lblSearchMateria;
        private System.Windows.Forms.Panel pnlTitleSection;
        private System.Windows.Forms.Label lblNameSection;
        private System.Windows.Forms.Panel pnlAddMateria;
        private System.Windows.Forms.Label lblDescriptionAddMateria;
        private System.Windows.Forms.PictureBox pbAddMateria;
        private System.Windows.Forms.Label lblAddMateria;
        private System.Windows.Forms.Panel pnlHelpSection;
        private System.Windows.Forms.PictureBox pbHelp;
        private System.Windows.Forms.Label lblDescriptionHelp;
        private System.Windows.Forms.Label lblHelp;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label6;
    }
}
