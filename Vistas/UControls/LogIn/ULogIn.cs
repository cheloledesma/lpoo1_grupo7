﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ClasesBase;

namespace Vistas.UControls.LogIn
{
    public partial class ULogIn : UserControl
    {
        private readonly Color _oColorLeaveOk = Color.FromArgb(22, 148, 94);
        private readonly Color _oColorHoverOk = Color.FromArgb(5, 82, 49);
        private readonly Color _oColorHoverCancel = Color.FromArgb(148, 105, 62);
        private readonly Color _oColorLeaveCancel = Color.FromArgb(217, 125, 33);
        
        public ULogIn()
        {
            InitializeComponent();
        }

        private void btnUlogInOk_MouseHover(object sender, EventArgs e)
        {
            btnUlogInOk.BackColor = _oColorHoverOk;
            btnUlogInOk.FlatAppearance.BorderSize = 1;
        }

        private void btnUlogInOk_MouseLeave(object sender, EventArgs e)
        {
            btnUlogInOk.BackColor = _oColorLeaveOk;
            btnUlogInOk.FlatAppearance.BorderSize = 0;
        }

        private void btnUlogInCancel_MouseHover(object sender, EventArgs e)
        {
            btnUlogInCancel.BackColor = _oColorHoverCancel;
            btnUlogInCancel.FlatAppearance.BorderSize = 1;
        }

        private void btnUlogInCancel_MouseLeave(object sender, EventArgs e)
        {
            btnUlogInCancel.BackColor = _oColorLeaveCancel;
            btnUlogInCancel.FlatAppearance.BorderSize = 0;
        }

        private void btnUlogInCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnUlogInOk_Click(object sender, EventArgs e)
        {
            var bUserFound = false;
            var oUser1 = new Usuario("juan", "123");
            var oUser2 = new Usuario("marta", "456");
            var oFrmMain = new frmMain();
            var szUsername = txtUsername.Text;
            var szPassword = txtPassword.Text;
            var szApellidoNombre = "";

            if (oUser1.UsuNombreUsuario.Equals(szUsername) && oUser1.UsuContrasena.Equals(szPassword))
            {
                bUserFound = true;
                szApellidoNombre = oUser1.UsuApellidoNombre;
            }

            if (oUser2.UsuNombreUsuario.Equals(szUsername) && oUser2.UsuContrasena.Equals(szPassword))
            {
                bUserFound = true;
                szApellidoNombre = oUser2.UsuApellidoNombre;
            }

            if (bUserFound)
            {
                this.Hide();
                //oFrmMain.lblApellidoNombreUser.Text = szApellidoNombre;
                //oFrmMain.lblStatusUser.Text = "Online";
                oFrmMain.ShowDialog();
            }
            else
            {
                MessageBox.Show("Datos de acceso incorrecto.", "Login incorrecto", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }

        }
    }
}
