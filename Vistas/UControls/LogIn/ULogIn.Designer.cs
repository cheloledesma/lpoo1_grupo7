﻿namespace Vistas.UControls.LogIn
{
    partial class ULogIn
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ULogIn));
            this.pnlUpUlogIn = new System.Windows.Forms.Panel();
            this.pbLogoUlogIn = new System.Windows.Forms.PictureBox();
            this.pnlBottomUlogIn = new System.Windows.Forms.Panel();
            this.pnlBottomUButtonLogIn = new System.Windows.Forms.Panel();
            this.btnUlogInOk = new System.Windows.Forms.Button();
            this.btnUlogInCancel = new System.Windows.Forms.Button();
            this.pnlBodyUlogIn = new System.Windows.Forms.Panel();
            this.pnlBodyUControlLogIn = new System.Windows.Forms.Panel();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.pbPassword = new System.Windows.Forms.PictureBox();
            this.pbUser = new System.Windows.Forms.PictureBox();
            this.pnlUpUlogIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogoUlogIn)).BeginInit();
            this.pnlBottomUlogIn.SuspendLayout();
            this.pnlBottomUButtonLogIn.SuspendLayout();
            this.pnlBodyUlogIn.SuspendLayout();
            this.pnlBodyUControlLogIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUser)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlUpUlogIn
            // 
            this.pnlUpUlogIn.Controls.Add(this.pbLogoUlogIn);
            this.pnlUpUlogIn.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUpUlogIn.Location = new System.Drawing.Point(0, 0);
            this.pnlUpUlogIn.Name = "pnlUpUlogIn";
            this.pnlUpUlogIn.Size = new System.Drawing.Size(700, 100);
            this.pnlUpUlogIn.TabIndex = 0;
            // 
            // pbLogoUlogIn
            // 
            this.pbLogoUlogIn.Image = ((System.Drawing.Image)(resources.GetObject("pbLogoUlogIn.Image")));
            this.pbLogoUlogIn.Location = new System.Drawing.Point(313, 10);
            this.pbLogoUlogIn.Name = "pbLogoUlogIn";
            this.pbLogoUlogIn.Size = new System.Drawing.Size(75, 80);
            this.pbLogoUlogIn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogoUlogIn.TabIndex = 0;
            this.pbLogoUlogIn.TabStop = false;
            // 
            // pnlBottomUlogIn
            // 
            this.pnlBottomUlogIn.Controls.Add(this.pnlBottomUButtonLogIn);
            this.pnlBottomUlogIn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottomUlogIn.Location = new System.Drawing.Point(0, 250);
            this.pnlBottomUlogIn.Name = "pnlBottomUlogIn";
            this.pnlBottomUlogIn.Size = new System.Drawing.Size(700, 100);
            this.pnlBottomUlogIn.TabIndex = 1;
            // 
            // pnlBottomUButtonLogIn
            // 
            this.pnlBottomUButtonLogIn.Controls.Add(this.btnUlogInOk);
            this.pnlBottomUButtonLogIn.Controls.Add(this.btnUlogInCancel);
            this.pnlBottomUButtonLogIn.Location = new System.Drawing.Point(175, 27);
            this.pnlBottomUButtonLogIn.Name = "pnlBottomUButtonLogIn";
            this.pnlBottomUButtonLogIn.Size = new System.Drawing.Size(350, 46);
            this.pnlBottomUButtonLogIn.TabIndex = 2;
            // 
            // btnUlogInOk
            // 
            this.btnUlogInOk.AutoSize = true;
            this.btnUlogInOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(148)))), ((int)(((byte)(94)))));
            this.btnUlogInOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUlogInOk.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnUlogInOk.FlatAppearance.BorderSize = 0;
            this.btnUlogInOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUlogInOk.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUlogInOk.ForeColor = System.Drawing.Color.White;
            this.btnUlogInOk.Location = new System.Drawing.Point(3, 8);
            this.btnUlogInOk.Name = "btnUlogInOk";
            this.btnUlogInOk.Size = new System.Drawing.Size(90, 30);
            this.btnUlogInOk.TabIndex = 0;
            this.btnUlogInOk.Text = "login";
            this.btnUlogInOk.UseVisualStyleBackColor = false;
            this.btnUlogInOk.Click += new System.EventHandler(this.btnUlogInOk_Click);
            this.btnUlogInOk.MouseLeave += new System.EventHandler(this.btnUlogInOk_MouseLeave);
            this.btnUlogInOk.MouseHover += new System.EventHandler(this.btnUlogInOk_MouseHover);
            // 
            // btnUlogInCancel
            // 
            this.btnUlogInCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(125)))), ((int)(((byte)(33)))));
            this.btnUlogInCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUlogInCancel.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnUlogInCancel.FlatAppearance.BorderSize = 0;
            this.btnUlogInCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUlogInCancel.Font = new System.Drawing.Font("Verdana", 11.25F);
            this.btnUlogInCancel.ForeColor = System.Drawing.Color.White;
            this.btnUlogInCancel.Location = new System.Drawing.Point(257, 8);
            this.btnUlogInCancel.Name = "btnUlogInCancel";
            this.btnUlogInCancel.Size = new System.Drawing.Size(90, 30);
            this.btnUlogInCancel.TabIndex = 1;
            this.btnUlogInCancel.Text = "cancel";
            this.btnUlogInCancel.UseVisualStyleBackColor = false;
            this.btnUlogInCancel.Click += new System.EventHandler(this.btnUlogInCancel_Click);
            this.btnUlogInCancel.MouseLeave += new System.EventHandler(this.btnUlogInCancel_MouseLeave);
            this.btnUlogInCancel.MouseHover += new System.EventHandler(this.btnUlogInCancel_MouseHover);
            // 
            // pnlBodyUlogIn
            // 
            this.pnlBodyUlogIn.Controls.Add(this.pnlBodyUControlLogIn);
            this.pnlBodyUlogIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBodyUlogIn.Location = new System.Drawing.Point(0, 100);
            this.pnlBodyUlogIn.Name = "pnlBodyUlogIn";
            this.pnlBodyUlogIn.Size = new System.Drawing.Size(700, 150);
            this.pnlBodyUlogIn.TabIndex = 2;
            // 
            // pnlBodyUControlLogIn
            // 
            this.pnlBodyUControlLogIn.Controls.Add(this.txtPassword);
            this.pnlBodyUControlLogIn.Controls.Add(this.txtUsername);
            this.pnlBodyUControlLogIn.Controls.Add(this.pbPassword);
            this.pnlBodyUControlLogIn.Controls.Add(this.pbUser);
            this.pnlBodyUControlLogIn.Location = new System.Drawing.Point(193, 25);
            this.pnlBodyUControlLogIn.Name = "pnlBodyUControlLogIn";
            this.pnlBodyUControlLogIn.Size = new System.Drawing.Size(315, 100);
            this.pnlBodyUControlLogIn.TabIndex = 0;
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(159)))), ((int)(((byte)(182)))));
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.Font = new System.Drawing.Font("Verdana", 18F);
            this.txtPassword.ForeColor = System.Drawing.Color.White;
            this.txtPassword.Location = new System.Drawing.Point(44, 67);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(10);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(264, 30);
            this.txtPassword.TabIndex = 3;
            this.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // txtUsername
            // 
            this.txtUsername.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(159)))), ((int)(((byte)(182)))));
            this.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUsername.Font = new System.Drawing.Font("Verdana", 18F);
            this.txtUsername.ForeColor = System.Drawing.Color.White;
            this.txtUsername.Location = new System.Drawing.Point(44, 3);
            this.txtUsername.Margin = new System.Windows.Forms.Padding(10);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(264, 30);
            this.txtUsername.TabIndex = 2;
            this.txtUsername.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pbPassword
            // 
            this.pbPassword.Image = ((System.Drawing.Image)(resources.GetObject("pbPassword.Image")));
            this.pbPassword.Location = new System.Drawing.Point(13, 67);
            this.pbPassword.Name = "pbPassword";
            this.pbPassword.Size = new System.Drawing.Size(30, 30);
            this.pbPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPassword.TabIndex = 1;
            this.pbPassword.TabStop = false;
            // 
            // pbUser
            // 
            this.pbUser.Image = ((System.Drawing.Image)(resources.GetObject("pbUser.Image")));
            this.pbUser.Location = new System.Drawing.Point(13, 3);
            this.pbUser.Name = "pbUser";
            this.pbUser.Size = new System.Drawing.Size(30, 30);
            this.pbUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbUser.TabIndex = 0;
            this.pbUser.TabStop = false;
            // 
            // ULogIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(59)))), ((int)(((byte)(75)))));
            this.Controls.Add(this.pnlBodyUlogIn);
            this.Controls.Add(this.pnlBottomUlogIn);
            this.Controls.Add(this.pnlUpUlogIn);
            this.Name = "ULogIn";
            this.Size = new System.Drawing.Size(700, 350);
            this.pnlUpUlogIn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogoUlogIn)).EndInit();
            this.pnlBottomUlogIn.ResumeLayout(false);
            this.pnlBottomUButtonLogIn.ResumeLayout(false);
            this.pnlBottomUButtonLogIn.PerformLayout();
            this.pnlBodyUlogIn.ResumeLayout(false);
            this.pnlBodyUControlLogIn.ResumeLayout(false);
            this.pnlBodyUControlLogIn.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUser)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlUpUlogIn;
        private System.Windows.Forms.PictureBox pbLogoUlogIn;
        private System.Windows.Forms.Panel pnlBottomUlogIn;
        private System.Windows.Forms.Button btnUlogInCancel;
        private System.Windows.Forms.Button btnUlogInOk;
        private System.Windows.Forms.Panel pnlBodyUlogIn;
        private System.Windows.Forms.Panel pnlBottomUButtonLogIn;
        private System.Windows.Forms.Panel pnlBodyUControlLogIn;
        private System.Windows.Forms.PictureBox pbPassword;
        private System.Windows.Forms.PictureBox pbUser;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUsername;
    }
}
