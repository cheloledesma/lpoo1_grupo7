﻿namespace Vistas
{
    partial class frmLogIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogIn));
            this.pnlUpLogIn = new System.Windows.Forms.Panel();
            this.pnlUpRightLogIn = new System.Windows.Forms.Panel();
            this.pbUpRightCloseLogIn = new System.Windows.Forms.PictureBox();
            this.pnlUpLeftLogIn = new System.Windows.Forms.Panel();
            this.pbUpLeftLogIn = new System.Windows.Forms.PictureBox();
            this.pnlBodyLogIn = new System.Windows.Forms.Panel();
            this.uCLogIn = new Vistas.UControls.LogIn.ULogIn();
            this.pnlUpLogIn.SuspendLayout();
            this.pnlUpRightLogIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUpRightCloseLogIn)).BeginInit();
            this.pnlUpLeftLogIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUpLeftLogIn)).BeginInit();
            this.pnlBodyLogIn.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlUpLogIn
            // 
            this.pnlUpLogIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(110)))), ((int)(((byte)(135)))));
            this.pnlUpLogIn.Controls.Add(this.pnlUpRightLogIn);
            this.pnlUpLogIn.Controls.Add(this.pnlUpLeftLogIn);
            this.pnlUpLogIn.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUpLogIn.ForeColor = System.Drawing.SystemColors.Control;
            this.pnlUpLogIn.Location = new System.Drawing.Point(0, 0);
            this.pnlUpLogIn.Name = "pnlUpLogIn";
            this.pnlUpLogIn.Size = new System.Drawing.Size(700, 50);
            this.pnlUpLogIn.TabIndex = 0;
            // 
            // pnlUpRightLogIn
            // 
            this.pnlUpRightLogIn.Controls.Add(this.pbUpRightCloseLogIn);
            this.pnlUpRightLogIn.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlUpRightLogIn.Location = new System.Drawing.Point(670, 0);
            this.pnlUpRightLogIn.Name = "pnlUpRightLogIn";
            this.pnlUpRightLogIn.Size = new System.Drawing.Size(30, 50);
            this.pnlUpRightLogIn.TabIndex = 1;
            // 
            // pbUpRightCloseLogIn
            // 
            this.pbUpRightCloseLogIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbUpRightCloseLogIn.Image = ((System.Drawing.Image)(resources.GetObject("pbUpRightCloseLogIn.Image")));
            this.pbUpRightCloseLogIn.Location = new System.Drawing.Point(5, 15);
            this.pbUpRightCloseLogIn.Name = "pbUpRightCloseLogIn";
            this.pbUpRightCloseLogIn.Size = new System.Drawing.Size(20, 20);
            this.pbUpRightCloseLogIn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbUpRightCloseLogIn.TabIndex = 1;
            this.pbUpRightCloseLogIn.TabStop = false;
            this.pbUpRightCloseLogIn.Click += new System.EventHandler(this.pbUpRightCloseLogIn_Click);
            // 
            // pnlUpLeftLogIn
            // 
            this.pnlUpLeftLogIn.Controls.Add(this.pbUpLeftLogIn);
            this.pnlUpLeftLogIn.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlUpLeftLogIn.Location = new System.Drawing.Point(0, 0);
            this.pnlUpLeftLogIn.Name = "pnlUpLeftLogIn";
            this.pnlUpLeftLogIn.Size = new System.Drawing.Size(30, 50);
            this.pnlUpLeftLogIn.TabIndex = 0;
            // 
            // pbUpLeftLogIn
            // 
            this.pbUpLeftLogIn.Image = ((System.Drawing.Image)(resources.GetObject("pbUpLeftLogIn.Image")));
            this.pbUpLeftLogIn.Location = new System.Drawing.Point(5, 15);
            this.pbUpLeftLogIn.Name = "pbUpLeftLogIn";
            this.pbUpLeftLogIn.Size = new System.Drawing.Size(20, 20);
            this.pbUpLeftLogIn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbUpLeftLogIn.TabIndex = 0;
            this.pbUpLeftLogIn.TabStop = false;
            // 
            // pnlBodyLogIn
            // 
            this.pnlBodyLogIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(59)))), ((int)(((byte)(75)))));
            this.pnlBodyLogIn.Controls.Add(this.uCLogIn);
            this.pnlBodyLogIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBodyLogIn.Location = new System.Drawing.Point(0, 50);
            this.pnlBodyLogIn.Name = "pnlBodyLogIn";
            this.pnlBodyLogIn.Size = new System.Drawing.Size(700, 350);
            this.pnlBodyLogIn.TabIndex = 1;
            // 
            // uCLogIn
            // 
            this.uCLogIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(59)))), ((int)(((byte)(75)))));
            this.uCLogIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uCLogIn.Location = new System.Drawing.Point(0, 0);
            this.uCLogIn.Name = "uCLogIn";
            this.uCLogIn.Size = new System.Drawing.Size(700, 350);
            this.uCLogIn.TabIndex = 0;
            // 
            // frmLogIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 400);
            this.Controls.Add(this.pnlBodyLogIn);
            this.Controls.Add(this.pnlUpLogIn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmLogIn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.pnlUpLogIn.ResumeLayout(false);
            this.pnlUpRightLogIn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbUpRightCloseLogIn)).EndInit();
            this.pnlUpLeftLogIn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbUpLeftLogIn)).EndInit();
            this.pnlBodyLogIn.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlUpLogIn;
        private System.Windows.Forms.Panel pnlUpLeftLogIn;
        private System.Windows.Forms.Panel pnlUpRightLogIn;
        private System.Windows.Forms.PictureBox pbUpLeftLogIn;
        private System.Windows.Forms.PictureBox pbUpRightCloseLogIn;
        public System.Windows.Forms.Panel pnlBodyLogIn;
        private UControls.LogIn.ULogIn uCLogIn;
        //private UControls.LogIn.ULogIn uLogIn;
        //private UControls.LogIn.ULogIn uCLogIn;
    }
}

