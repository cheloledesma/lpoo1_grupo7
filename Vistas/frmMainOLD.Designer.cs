﻿namespace Vistas
{
    partial class frmMainOLD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainOLD));
            this.pnlLeftMain = new System.Windows.Forms.Panel();
            this.pnlLeftBottomMain = new System.Windows.Forms.Panel();
            this.mnuLeftMain = new System.Windows.Forms.MenuStrip();
            this.mnuItemSistema = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemRecursos = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemProfesores = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemMaterias = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemReservas = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlDatosUsuarioConectado = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblStatusUser = new System.Windows.Forms.Label();
            this.pbImageUser = new System.Windows.Forms.PictureBox();
            this.lblApellidoNombreUser = new System.Windows.Forms.Label();
            this.pnlUpLeft = new System.Windows.Forms.Panel();
            this.lblNameSystem = new System.Windows.Forms.Label();
            this.pnlUpMain = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbCloseMain = new System.Windows.Forms.PictureBox();
            this.pnlBodyMain = new System.Windows.Forms.Panel();
            this.sttStripBottom = new System.Windows.Forms.StatusStrip();
            this.tlsStatusLabelCopyright = new System.Windows.Forms.ToolStripStatusLabel();
            this.tlsStatusLabelSystem = new System.Windows.Forms.ToolStripStatusLabel();
            this.tlsStatusLabelVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlLeftMain.SuspendLayout();
            this.pnlLeftBottomMain.SuspendLayout();
            this.mnuLeftMain.SuspendLayout();
            this.pnlDatosUsuarioConectado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImageUser)).BeginInit();
            this.pnlUpLeft.SuspendLayout();
            this.pnlUpMain.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCloseMain)).BeginInit();
            this.pnlBodyMain.SuspendLayout();
            this.sttStripBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlLeftMain
            // 
            this.pnlLeftMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(54)))), ((int)(((byte)(79)))));
            this.pnlLeftMain.Controls.Add(this.pnlLeftBottomMain);
            this.pnlLeftMain.Controls.Add(this.pnlDatosUsuarioConectado);
            this.pnlLeftMain.Controls.Add(this.pnlUpLeft);
            this.pnlLeftMain.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeftMain.Location = new System.Drawing.Point(0, 0);
            this.pnlLeftMain.Name = "pnlLeftMain";
            this.pnlLeftMain.Size = new System.Drawing.Size(200, 410);
            this.pnlLeftMain.TabIndex = 0;
            // 
            // pnlLeftBottomMain
            // 
            this.pnlLeftBottomMain.Controls.Add(this.mnuLeftMain);
            this.pnlLeftBottomMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLeftBottomMain.Location = new System.Drawing.Point(0, 138);
            this.pnlLeftBottomMain.Name = "pnlLeftBottomMain";
            this.pnlLeftBottomMain.Size = new System.Drawing.Size(200, 272);
            this.pnlLeftBottomMain.TabIndex = 6;
            // 
            // mnuLeftMain
            // 
            this.mnuLeftMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(54)))), ((int)(((byte)(79)))));
            this.mnuLeftMain.Dock = System.Windows.Forms.DockStyle.Left;
            this.mnuLeftMain.Font = new System.Drawing.Font("Calibri", 12F);
            this.mnuLeftMain.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mnuLeftMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemSistema,
            this.mnuItemRecursos,
            this.mnuItemProfesores,
            this.mnuItemMaterias,
            this.mnuItemReservas});
            this.mnuLeftMain.Location = new System.Drawing.Point(0, 0);
            this.mnuLeftMain.Margin = new System.Windows.Forms.Padding(10);
            this.mnuLeftMain.Name = "mnuLeftMain";
            this.mnuLeftMain.Padding = new System.Windows.Forms.Padding(5, 5, 0, 2);
            this.mnuLeftMain.Size = new System.Drawing.Size(115, 272);
            this.mnuLeftMain.TabIndex = 0;
            // 
            // mnuItemSistema
            // 
            this.mnuItemSistema.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.mnuItemSistema.ForeColor = System.Drawing.Color.White;
            this.mnuItemSistema.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemSistema.Image")));
            this.mnuItemSistema.Name = "mnuItemSistema";
            this.mnuItemSistema.Size = new System.Drawing.Size(104, 24);
            this.mnuItemSistema.Text = "Sistema";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(54)))), ((int)(((byte)(79)))));
            this.salirToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.salirToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("salirToolStripMenuItem.Image")));
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(110, 26);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // mnuItemRecursos
            // 
            this.mnuItemRecursos.ForeColor = System.Drawing.Color.White;
            this.mnuItemRecursos.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemRecursos.Image")));
            this.mnuItemRecursos.Name = "mnuItemRecursos";
            this.mnuItemRecursos.Size = new System.Drawing.Size(104, 24);
            this.mnuItemRecursos.Text = "Recursos";
            // 
            // mnuItemProfesores
            // 
            this.mnuItemProfesores.ForeColor = System.Drawing.Color.White;
            this.mnuItemProfesores.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemProfesores.Image")));
            this.mnuItemProfesores.Name = "mnuItemProfesores";
            this.mnuItemProfesores.Size = new System.Drawing.Size(104, 24);
            this.mnuItemProfesores.Text = "Profesores";
            // 
            // mnuItemMaterias
            // 
            this.mnuItemMaterias.ForeColor = System.Drawing.Color.White;
            this.mnuItemMaterias.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemMaterias.Image")));
            this.mnuItemMaterias.Name = "mnuItemMaterias";
            this.mnuItemMaterias.Size = new System.Drawing.Size(104, 24);
            this.mnuItemMaterias.Text = "Materias";
            // 
            // mnuItemReservas
            // 
            this.mnuItemReservas.ForeColor = System.Drawing.Color.White;
            this.mnuItemReservas.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemReservas.Image")));
            this.mnuItemReservas.Name = "mnuItemReservas";
            this.mnuItemReservas.Size = new System.Drawing.Size(104, 24);
            this.mnuItemReservas.Text = "Reservas";
            // 
            // pnlDatosUsuarioConectado
            // 
            this.pnlDatosUsuarioConectado.Controls.Add(this.pictureBox1);
            this.pnlDatosUsuarioConectado.Controls.Add(this.lblStatusUser);
            this.pnlDatosUsuarioConectado.Controls.Add(this.pbImageUser);
            this.pnlDatosUsuarioConectado.Controls.Add(this.lblApellidoNombreUser);
            this.pnlDatosUsuarioConectado.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDatosUsuarioConectado.Location = new System.Drawing.Point(0, 40);
            this.pnlDatosUsuarioConectado.Name = "pnlDatosUsuarioConectado";
            this.pnlDatosUsuarioConectado.Size = new System.Drawing.Size(200, 98);
            this.pnlDatosUsuarioConectado.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(68, 43);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(15, 15);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // lblStatusUser
            // 
            this.lblStatusUser.AutoSize = true;
            this.lblStatusUser.Font = new System.Drawing.Font("Calibri", 9F);
            this.lblStatusUser.ForeColor = System.Drawing.Color.White;
            this.lblStatusUser.Location = new System.Drawing.Point(89, 44);
            this.lblStatusUser.Name = "lblStatusUser";
            this.lblStatusUser.Size = new System.Drawing.Size(81, 14);
            this.lblStatusUser.TabIndex = 4;
            this.lblStatusUser.Text = "lblStatusUser";
            // 
            // pbImageUser
            // 
            this.pbImageUser.Image = ((System.Drawing.Image)(resources.GetObject("pbImageUser.Image")));
            this.pbImageUser.Location = new System.Drawing.Point(12, 21);
            this.pbImageUser.Name = "pbImageUser";
            this.pbImageUser.Size = new System.Drawing.Size(50, 50);
            this.pbImageUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImageUser.TabIndex = 3;
            this.pbImageUser.TabStop = false;
            // 
            // lblApellidoNombreUser
            // 
            this.lblApellidoNombreUser.AutoSize = true;
            this.lblApellidoNombreUser.Font = new System.Drawing.Font("Calibri", 9F);
            this.lblApellidoNombreUser.ForeColor = System.Drawing.Color.White;
            this.lblApellidoNombreUser.Location = new System.Drawing.Point(68, 21);
            this.lblApellidoNombreUser.Name = "lblApellidoNombreUser";
            this.lblApellidoNombreUser.Size = new System.Drawing.Size(137, 14);
            this.lblApellidoNombreUser.TabIndex = 2;
            this.lblApellidoNombreUser.Text = "lblApellidoNombreUser";
            // 
            // pnlUpLeft
            // 
            this.pnlUpLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(100)))), ((int)(((byte)(200)))));
            this.pnlUpLeft.Controls.Add(this.lblNameSystem);
            this.pnlUpLeft.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUpLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlUpLeft.Name = "pnlUpLeft";
            this.pnlUpLeft.Size = new System.Drawing.Size(200, 40);
            this.pnlUpLeft.TabIndex = 0;
            // 
            // lblNameSystem
            // 
            this.lblNameSystem.AutoSize = true;
            this.lblNameSystem.Enabled = false;
            this.lblNameSystem.Font = new System.Drawing.Font("Calibri", 20F);
            this.lblNameSystem.ForeColor = System.Drawing.Color.White;
            this.lblNameSystem.Location = new System.Drawing.Point(29, 4);
            this.lblNameSystem.Name = "lblNameSystem";
            this.lblNameSystem.Size = new System.Drawing.Size(142, 33);
            this.lblNameSystem.TabIndex = 4;
            this.lblNameSystem.Text = "G7 Systems";
            // 
            // pnlUpMain
            // 
            this.pnlUpMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(121)))), ((int)(((byte)(251)))));
            this.pnlUpMain.Controls.Add(this.panel1);
            this.pnlUpMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUpMain.Location = new System.Drawing.Point(200, 0);
            this.pnlUpMain.Name = "pnlUpMain";
            this.pnlUpMain.Size = new System.Drawing.Size(521, 40);
            this.pnlUpMain.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pbCloseMain);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(481, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(40, 40);
            this.panel1.TabIndex = 0;
            // 
            // pbCloseMain
            // 
            this.pbCloseMain.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbCloseMain.Image = ((System.Drawing.Image)(resources.GetObject("pbCloseMain.Image")));
            this.pbCloseMain.Location = new System.Drawing.Point(10, 10);
            this.pbCloseMain.Name = "pbCloseMain";
            this.pbCloseMain.Size = new System.Drawing.Size(20, 20);
            this.pbCloseMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCloseMain.TabIndex = 0;
            this.pbCloseMain.TabStop = false;
            this.pbCloseMain.Click += new System.EventHandler(this.pbCloseMain_Click);
            // 
            // pnlBodyMain
            // 
            this.pnlBodyMain.Controls.Add(this.sttStripBottom);
            this.pnlBodyMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBodyMain.Location = new System.Drawing.Point(200, 40);
            this.pnlBodyMain.Name = "pnlBodyMain";
            this.pnlBodyMain.Size = new System.Drawing.Size(521, 370);
            this.pnlBodyMain.TabIndex = 2;
            // 
            // sttStripBottom
            // 
            this.sttStripBottom.Font = new System.Drawing.Font("Calibri", 9F);
            this.sttStripBottom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlsStatusLabelCopyright,
            this.tlsStatusLabelSystem,
            this.tlsStatusLabelVersion});
            this.sttStripBottom.Location = new System.Drawing.Point(0, 348);
            this.sttStripBottom.Name = "sttStripBottom";
            this.sttStripBottom.Size = new System.Drawing.Size(521, 22);
            this.sttStripBottom.TabIndex = 0;
            // 
            // tlsStatusLabelCopyright
            // 
            this.tlsStatusLabelCopyright.Name = "tlsStatusLabelCopyright";
            this.tlsStatusLabelCopyright.Size = new System.Drawing.Size(156, 17);
            this.tlsStatusLabelCopyright.Text = "Copyright 2018 - Powered by";
            // 
            // tlsStatusLabelSystem
            // 
            this.tlsStatusLabelSystem.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.tlsStatusLabelSystem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(121)))), ((int)(((byte)(251)))));
            this.tlsStatusLabelSystem.Name = "tlsStatusLabelSystem";
            this.tlsStatusLabelSystem.Size = new System.Drawing.Size(57, 17);
            this.tlsStatusLabelSystem.Text = "LPOOI_G7";
            // 
            // tlsStatusLabelVersion
            // 
            this.tlsStatusLabelVersion.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.tlsStatusLabelVersion.Name = "tlsStatusLabelVersion";
            this.tlsStatusLabelVersion.Size = new System.Drawing.Size(70, 17);
            this.tlsStatusLabelVersion.Text = "Versión 0.0.1";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 410);
            this.Controls.Add(this.pnlBodyMain);
            this.Controls.Add(this.pnlUpMain);
            this.Controls.Add(this.pnlLeftMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.mnuLeftMain;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.pnlLeftMain.ResumeLayout(false);
            this.pnlLeftBottomMain.ResumeLayout(false);
            this.pnlLeftBottomMain.PerformLayout();
            this.mnuLeftMain.ResumeLayout(false);
            this.mnuLeftMain.PerformLayout();
            this.pnlDatosUsuarioConectado.ResumeLayout(false);
            this.pnlDatosUsuarioConectado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImageUser)).EndInit();
            this.pnlUpLeft.ResumeLayout(false);
            this.pnlUpLeft.PerformLayout();
            this.pnlUpMain.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbCloseMain)).EndInit();
            this.pnlBodyMain.ResumeLayout(false);
            this.pnlBodyMain.PerformLayout();
            this.sttStripBottom.ResumeLayout(false);
            this.sttStripBottom.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlLeftMain;
        private System.Windows.Forms.Panel pnlUpMain;
        private System.Windows.Forms.Panel pnlBodyMain;
        private System.Windows.Forms.Panel pnlUpLeft;
        private System.Windows.Forms.Label lblNameSystem;
        public System.Windows.Forms.Panel pnlDatosUsuarioConectado;
        private System.Windows.Forms.PictureBox pbImageUser;
        public System.Windows.Forms.Label lblApellidoNombreUser;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.StatusStrip sttStripBottom;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pbCloseMain;
        private System.Windows.Forms.ToolStripStatusLabel tlsStatusLabelCopyright;
        private System.Windows.Forms.ToolStripStatusLabel tlsStatusLabelSystem;
        private System.Windows.Forms.ToolStripStatusLabel tlsStatusLabelVersion;
        private System.Windows.Forms.Panel pnlLeftBottomMain;
        private System.Windows.Forms.MenuStrip mnuLeftMain;
        private System.Windows.Forms.ToolStripMenuItem mnuItemSistema;
        private System.Windows.Forms.ToolStripMenuItem mnuItemRecursos;
        private System.Windows.Forms.ToolStripMenuItem mnuItemProfesores;
        private System.Windows.Forms.ToolStripMenuItem mnuItemMaterias;
        private System.Windows.Forms.ToolStripMenuItem mnuItemReservas;
        public System.Windows.Forms.Label lblStatusUser;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
    }
}