﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vistas
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnSistema_Click(object sender, EventArgs e)
        {
            ViewPnlLeftMin(btnSistema);
            pnlBody.Controls.Clear();
            UControls.Sistema.UcMenuSistema oUcMenuSistema = new UControls.Sistema.UcMenuSistema();
            pnlBody.Controls.Add(oUcMenuSistema);
            oUcMenuSistema.Dock = DockStyle.Fill;
        }

        public void ViewPnlLeftMin(Button oButtomClick)
        {
            pnlLeftMin.Height = oButtomClick.Height;
            pnlLeftMin.Top = oButtomClick.Top;
        }

        private void btnRecursos_Click(object sender, EventArgs e)
        {
            ViewPnlLeftMin(btnRecursos);
            pnlBody.Controls.Clear();
            UControls.Recursos.UcMenuRecursos oUcMenuRecursos = new UControls.Recursos.UcMenuRecursos();
            pnlBody.Controls.Add(oUcMenuRecursos);
            oUcMenuRecursos.Dock = DockStyle.Fill;
        }

        private void btnProfesores_Click(object sender, EventArgs e)
        {
            ViewPnlLeftMin(btnProfesores);
            pnlBody.Controls.Clear();
            UControls.Profesores.UcMenuProfesores oUcMenuProfesores = new UControls.Profesores.UcMenuProfesores();
            pnlBody.Controls.Add(oUcMenuProfesores);
            oUcMenuProfesores.Dock = DockStyle.Fill;
        }

        private void btnMaterias_Click(object sender, EventArgs e)
        {
            ViewPnlLeftMin(btnMaterias);
            pnlBody.Controls.Clear();
            UControls.Materias.UcMenuMaterias oUcMenuMaterias = new UControls.Materias.UcMenuMaterias();
            pnlBody.Controls.Add(oUcMenuMaterias);
            oUcMenuMaterias.Dock = DockStyle.Fill;
        }

        private void btnReservas_Click(object sender, EventArgs e)
        {
            ViewPnlLeftMin(btnReservas);
            pnlBody.Controls.Clear();
            UControls.Reservas.UcMenuReservas oUcMenuReservas = new UControls.Reservas.UcMenuReservas();
            pnlBody.Controls.Add(oUcMenuReservas);
            oUcMenuReservas.Dock = DockStyle.Fill;
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            ViewPnlLeftMin(btnHome);
            pnlBody.Controls.Clear();
            UControls.UcMenuHome oUcMenuHome = new UControls.UcMenuHome();
            pnlBody.Controls.Add(oUcMenuHome);
            oUcMenuHome.Dock = DockStyle.Fill;
        }

        private void frmMain2_Load(object sender, EventArgs e)
        {
            ViewPnlLeftMin(btnHome);
            pnlBody.Controls.Clear();
            UControls.UcMenuHome oUcMenuHome = new UControls.UcMenuHome();
            pnlBody.Controls.Add(oUcMenuHome);
            oUcMenuHome.Dock = DockStyle.Fill;
        }
    }
}
