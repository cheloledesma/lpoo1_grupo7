﻿namespace ClasesBase
{
    public class Usuario
    {
        private int _usuId;
        private string _usuNombreUsuario;
        private string _usuContrasena;
        private string _usuApellidoNombre;
        private string _usuRol;

        public int UsuId
        {
            get { return _usuId; }
            set { _usuId = value; }
        }
        public string UsuNombreUsuario
        {
            get { return _usuNombreUsuario; }
            set { _usuNombreUsuario = value; }
        }
        public string UsuContrasena
        {
            get { return _usuContrasena; }
            set { _usuContrasena = value; }
        }
        public string UsuApellidoNombre
        {
            get { return _usuApellidoNombre; }
            set { _usuApellidoNombre = value; }
        }
        public string UsuRol
        {
            get { return _usuRol; }
            set { _usuRol = value; }
        }

        public Usuario(string username, string password)
        {
            UsuNombreUsuario = username;
            UsuContrasena = password;
            UsuApellidoNombre = "Juárez Marta";
            if (username.Equals("juan"))
            {
                UsuApellidoNombre = "Pérez Juan";
            }
        }
    }
}
