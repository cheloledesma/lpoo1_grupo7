﻿namespace ClasesBase
{
    public class Profesor
    {

        private int _proLegajo;
        private string _proApellido;
        private string _proNombre;

        public int ProLegajo
        {
            get { return _proLegajo; }
            set { _proLegajo = value; }
        }
        public string ProApellido
        {
            get { return _proApellido; }
            set { _proApellido = value; }
        }
        public string ProNombre
        {
            get { return _proNombre; }
            set { _proNombre = value; }
        }
    }
}
