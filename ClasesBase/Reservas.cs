﻿using System;
namespace ClasesBase
{
    public class Reservas
    {
        private int _resId;
        private int _recId;
        private int _proLgajo;
        private int _matId;
        private DateTime _resFechaHora;

        public int ResId
        {
            get { return _resId; }
            set { _resId = value; }
        }
        public int RecId
        {
            get { return _recId; }
            set { _recId = value; }
        }
        public int ProLgajo
        {
            get { return _proLgajo; }
            set { _proLgajo = value; }
        }
        public int MatId
        {
            get { return _matId; }
            set { _matId = value; }
        }
        public DateTime ResFechaHora
        {
            get { return _resFechaHora; }
            set { _resFechaHora = value; }
        }
    }
}
