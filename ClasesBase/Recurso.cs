﻿namespace ClasesBase
{
    public class Recurso
    {
        private int _recId;
        private string _recDescripcion;

        public int RecId
        {
            get { return _recId; }
            set { _recId = value; }
        }
        public string RecDescripcion
        {
            get { return _recDescripcion; }
            set { _recDescripcion = value; }
        }
    }
}
