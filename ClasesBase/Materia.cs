﻿namespace ClasesBase
{
    public class Materia
    {
        private int _matId;
        private string _matNombre;
        private string _matCarrera;

        public int MatId
        {
            get { return _matId; }
            set { _matId = value; }
        }
        public string MatNombre
        {
            get { return _matNombre; }
            set { _matNombre = value; }
        }
        public string MatCarrera
        {
            get { return _matCarrera; }
            set { _matCarrera = value; }
        }
    }
}
